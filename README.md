# My MLOPs project for ODS course


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/zhenyabratkovski5/mlops_2024.git
git branch -M main
git push -uf origin main
```
## What has been accomplished:
* Publish repository to gitlab (https://gitlab.com/zhenyabratkovski5/mlops_2024);
* Choose linter and formatter (RUFF is chosen);
* Fix the necessary dependencies for the linters (done in pyproject.toml and additionally in requirements.txt);
* Configure pre-commit in repository (configured, yaml file created);
* Configure linters and formatters in pyproject.toml, specify basic parameters of tools(done);
* Fix in contributing.md how to use linters in your project(done, I use Ruff);
* add to readme.md the methodology for maintaining your repository(done);
